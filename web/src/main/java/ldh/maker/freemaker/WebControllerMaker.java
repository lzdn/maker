package ldh.maker.freemaker;

import ldh.common.PageResult;
import ldh.common.json.ValuedEnumObjectSerializer;
import ldh.database.Column;
import ldh.maker.util.EnumFactory;
import ldh.maker.util.FreeMakerUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class WebControllerMaker extends BeanMaker<WebControllerMaker> {
	
	private Class<?> bean;
	private BeanMaker<?> beanWhereMaker;
	private BeanMaker<?> beanMaker;
	private ServiceInterfaceMaker serviceInterfaceMaker = null;
	private ServiceMaker serviceMaker = null;
	private String ftl = null;
	private Boolean isShiro = false;
	private List<Map<String, Object>> enumMakerList = new ArrayList<>();
	
	public WebControllerMaker() {
		imports.add(PageResult.class.getName());
		imports.add(Controller.class.getName());
		imports.add(Resource.class.getName());
		imports.add(ModelAttribute.class.getName());
		imports.add(PathVariable.class.getName());
		imports.add(RequestMapping.class.getName());
		imports.add(RequestMethod.class.getName());
		imports.add(Model.class.getName());
		imports.add(Objects.class.getName());
		imports.add("ldh.common.json.JsonViewFactory");
		imports.add(ResponseBody.class.getName());

	}
	
	public WebControllerMaker service(ServiceInterfaceMaker sm) {
		serviceInterfaceMaker = sm;
		this.imports.add(serviceInterfaceMaker.getName());
		return this;
	}
	
	public WebControllerMaker service(ServiceMaker sm) {
		serviceMaker = sm;
		this.imports.add(serviceMaker.getName());
		return this;
	}
	
	public WebControllerMaker bean(Class<?> bean) {
		this.bean = bean;
		this.imports.add(bean.getName());
		return this;
	}

	public WebControllerMaker ftl(String ftl) {
		this.ftl = ftl;
		return this;
	}

	public WebControllerMaker shiro(Boolean isShiro) {
		this.isShiro = isShiro;
		if (isShiro) {
			imports(RequiresPermissions.class);
		}
		return this;
	}
	
	public WebControllerMaker beanWhereMaker(BeanMaker<?> beanWhereMaker) {
		this.beanWhereMaker = beanWhereMaker;
		this.imports.add(beanWhereMaker.getName());
		return this;
	}
	
	public WebControllerMaker beanMaker(BeanMaker<?> beanMaker) {
		this.beanMaker = beanMaker;
		this.imports.add(beanMaker.getName());
		return this;
	}
	
	public void data() {
		check();
		if (bean != null) {
			data.put("bean", bean.getSimpleName());
			data.put("beanWhere", bean.getSimpleName() + "Where");
			data.put("lowerBean", FreeMakerUtil.firstLower(bean.getSimpleName()));
			
		}
		if (beanWhereMaker != null){
			data.put("beanWhere", beanWhereMaker.getSimpleName() + "Where");
			data.put("beanWhereMaker", beanWhereMaker);
		}
		
		if (beanMaker != null){
			data.put("bean", beanMaker.getSimpleName());
			data.put("lowerBean", FreeMakerUtil.firstLower(beanMaker.getSimpleName()));
		}
		if (serviceInterfaceMaker != null) {
			data.put("service", serviceInterfaceMaker);
		} else {
			data.put("service", serviceMaker);
		}
		data.put("module", FreeMakerUtil.firstLower(table.getJavaName()));
		
		String t = FreeMakerUtil.firstLower(table.getJavaName());
//		StringBuilder sb = new StringBuilder();
//		for (int i=0; i<t.length(); i++) {
//			if (Character.isUpperCase(t.charAt(i))) {
//				sb.append("_");
//			}
//			sb.append(t.substring(i, i+1).toLowerCase());
//		}
		data.put("jspFile", t.toLowerCase());
		data.put("shiro", isShiro);

		if (FreeMakerUtil.hasDate(table)) {
			this.imports.add("ldh.common.mvc.DateEditor");
		}
		handleEnum();

		for (Column column : table.getColumnList()) {
			if (FreeMakerUtil.isEnum(column)) {
				String keyId = treeId + "_" + dbName + "_" + table.getName() + "_" + column.getName();
				EnumStatusMaker enumStatusMaker = EnumFactory.getInstance().get(keyId);
				Map<String, Object> data = new HashMap<>();
				data.put("column", column);
				data.put("enumClassName", enumStatusMaker.getClassName());
				enumMakerList.add(data);
			}
		}

		data.put("enumMakerList", enumMakerList);

		fileName = this.className + ".java";
		super.data();
	}
	
	public void check() {
		if (bean == null && beanWhereMaker == null) {
			throw new NullPointerException("bean or beanMaker is not null!!!");
		}
		if (table == null && table == null) {
			throw new NullPointerException("tabe is not null!!!");
		}
		
		if(FreeMakerUtil.hasDate(table)) {
			imports.add(Date.class.getName());
			imports.add(InitBinder.class.getName());
//			imports.add(DateEditor.class.getName());
			imports.add(ServletRequestDataBinder.class.getName());
			imports.add(HttpServletRequest.class.getName());
		}
		
		super.check();
	}
	
	@Override
	public WebControllerMaker make() {
		data();
		if (ftl != null) {
			out(ftl, data);
		} else {
			out("controller.ftl", data);
		}
		
		return this;
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
		
		new WebControllerMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className("ScenicDao")
		 	.make();
		
	}
}
