package ldh.maker;

import ldh.maker.component.VertxContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/6/23.
 */
public class VertxMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new VertxContentUiFactory());
    }

    @Override
    protected String getTitle() {
        return "Vertx + sync 前后端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}
