package ldh.maker.freemaker;

import ldh.database.Table;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxMainControllerMaker extends FreeMarkerMaker<JavafxMainControllerMaker> {

    protected Table table;
    protected String projectPackage;
    protected String pojoPackage;
    protected String servicePackage;
    protected String pack;
    protected String enumProjectPackage;

    public JavafxMainControllerMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxMainControllerMaker pack(String pack) {
        this.pack = pack;
        return this;
    }

    public JavafxMainControllerMaker servicePackage(String servicePackage) {
        this.servicePackage = servicePackage;
        return this;
    }

    public JavafxMainControllerMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxMainControllerMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    public JavafxMainControllerMaker enumProjectPackage(String enumProjectPackage) {
        this.enumProjectPackage = enumProjectPackage;
        return this;
    }

    @Override
    public JavafxMainControllerMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "MainController.java";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("pojoPackage", pojoPackage);
        data.put("controllerPackage", pack);
        data.put("servicePackage", servicePackage);
        data.put("enumProjectPackage", enumProjectPackage);
    }
}
