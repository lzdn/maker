package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.EnumFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;

import java.util.List;

/**
 * Created by ldh123 on 2018/5/27.
 */
public class SpringJavafxCreateCode extends JavafxCreateCode {

    public SpringJavafxCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    @Override
    public String getProjectName() {
        return "desktop-spring";
    }

    @Override
    protected void createOther(){
        if (data == null) return;
        javafxPath = data.getPojoPackageProperty();
        String pojoPath = createPath(javafxPath);
        if (!table.isCreate()) return;

        if (table.isMiddle()) {
            return;
        }

        Class<?> key = null;
        if (table.getPrimaryKey() != null && !table.getPrimaryKey().isComposite()) {
            Column c = table.getPrimaryKey().getColumns().iterator().next();
            key = c.getPropertyClass();
        }

        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        JavafxPojoMaker javafxPojoMaker = buildJavafxPojoMaker(table, pojoPath, key, null);

        TableViewData tableViewData = tableInfo.getTableViewDataMap().get(table.getName());
        List<TableViewData.ColumnData> columnDatas = tableViewData.getColumnDatas();

        for (Column column : table.getColumnList()) {
            boolean isHave = false;
            for (TableViewData.ColumnData columnData : columnDatas) {
                if (columnData.getColumnName().equals(column.getName())) {
                    isHave = true;
                    column.setCreate(columnData.getShow());
                    column.setWidth(columnData.getWidth());
                }
            }
            if (!isHave) continue;

            String keyId = treeItem.getParent().getValue().getId() + "_" + dbName + "_" + table.getName() + "_" + column.getName();
            EnumStatusMaker enumStatusMaker = EnumFactory.getInstance().get(keyId);
            if (enumStatusMaker != null) {
                String p = createPath(enumStatusMaker.getPack());
                enumStatusMaker.outPath(p).make();
                javafxPojoMaker.imports(enumStatusMaker);
                column.setJavaType(enumStatusMaker.getSimpleName());
            }
        }

        copyResource();
        buildPomXmlMaker();
        buildApplicationPropertiesMaker();
        buildLog4jPropertiesMaker(javafxPath);
        buildJavafxMainMaker(tableInfo);
        buildJavafxMaker(tableInfo);
        buildUtilFile(tableInfo, "cell", "DateTableCellFactory.ftl", "DateTableCellFactory.java");
        buildUtilFile(tableInfo, "cell", "ObjectTableCellFactory.ftl", "ObjectTableCellFactory.java");

        if (table.getPrimaryKey() != null && !table.getPrimaryKey().isComposite()) {
            buildJavafxMainFormMaker(table, "spring/PojoMain.ftl");
            buildJavafxMainControllerMaker(table, "spring/PojoMainController.ftl");
            buildJavafxEditFormMaker(table, "spring/PojoEditForm.ftl");
            buildJavafxEditControllerMaker(table, "spring/PojoEditController.ftl");
            buildJavafxSearchFormMaker(table, "spring/PojoSearchForm.ftl");
            buildJavafxSearchControllerMaker(table, "spring/PojoSearchController.ftl");
        }
    }

    protected void buildPomXmlMaker() {
        String projectRootPackage = getProjectRootPackage(javafxPath);
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .project(this.getProjectName())
                .outPath(resourcePath)
                .ftl("spring/pom.ftl")
                .make();
    }

    protected void buildJavafxMainMaker(TableInfo tableInfo) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage);
        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("spring/DeskSpringMain.ftl")
                .fileName("DeskSpringMain.java")
                .outPath(path)
                .make();
    }

    protected void buildJavafxMaker(TableInfo tableInfo) {
        String fxml = createResourcePath("resources", "fxml");
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller");
        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("Home.ftl")
                .fileName("Home.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("spring/HomeController.ftl")
                .fileName("HomeController.java")
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(projectPackage + ".controller")
                .table(table)
                .tableInfo(tableInfo)
                .className("HomeView")
                .outPath(path)
                .fxml("/fxml/Home.fxml")
                .fileName("HomeView.java")
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("HomeHead.ftl")
                .fileName("HomeHead.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("spring/HomeHeadController.ftl")
                .fileName("HomeHeadController.java")
                .outPath(path)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .table(table)
                .ftl("HomeLeft.ftl")
                .fileName("HomeLeft.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("spring/HomeLeftController.ftl")
                .fileName("HomeLeftController.java")
                .outPath(path)
                .make();
    }

    protected void buildApplicationPropertiesMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("spring/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();
    }

    protected void buildJavafxSearchControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxSearchControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .enumProjectPackage(getEnumProjectPackage())
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "SearchFormView")
                .fileName(table.getJavaName() + "SearchFormView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "SearchForm.fxml")
                .make();
    }

    @Override
    protected void buildJavafxEditControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        String enumPackage = getEnumProjectPackage();

        new JavafxEditControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .enumProjectPackage(enumPackage)
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "EditFormView")
                .fileName(table.getJavaName() + "EditFormView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "EditForm.fxml")
                .make();
    }

    @Override
    protected void buildJavafxMainControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxMainControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .enumProjectPackage(getEnumProjectPackage())
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .servicePackage(data.getServicePackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();

        new JavafxViewMaker()
                .pack(data.getControllerPackageProperty() + "." + FreeMakerUtil.firstLower(table.getJavaName()))
                .table(table)
                .className(table.getJavaName() + "MainView")
                .fileName(table.getJavaName() + "MainView.java")
                .outPath(path)
                .fxml("/fxml/module/" + FreeMakerUtil.firstLower(table.getJavaName()) + "/" + table.getJavaName() + "Main.fxml")
                .make();
    }

    protected void buildApplicationBootMaker() {

    }

    protected void buildEnumController(){}
}
