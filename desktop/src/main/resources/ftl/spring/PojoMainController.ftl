package ${controllerPackage}.${util.firstLower(table.javaName)};

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.Region;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.Function;
import java.util.Map;
import java.util.HashMap;
import org.apache.commons.beanutils.BeanUtils;
import ldh.common.PageResult;
import ldh.common.Pageable;
import ldh.fx.StageUtil;
import ldh.fx.component.LxDialog;
import ldh.fx.component.table.GridTable;
import ldh.fx.component.table.function.LoadData;
import ldh.fx.util.DialogUtil;

import ${pojoPackage}.${table.javaName};
import ${pojoPackage}.where.${table.javaName}Where;
<#list table.columnList as column>
    <#if util.isEnum(column)>
import ${enumProjectPackage}.${column.javaType};
    </#if>
</#list>
import ${servicePackage}.${table.javaName}Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

/**
* Created by ldh on 2017/4/17.
*/
@FXMLController
public class ${table.javaName}MainController implements LoadData,  Initializable{

    @FXML private GridTable<${table.javaName}> ${util.firstLower(table.javaName)}GridTable;

    @Resource
    private ${table.javaName}EditFormView ${util.firstLower(table.javaName)}EditFormView;
    @Resource
    private ${table.javaName}SearchFormView ${util.firstLower(table.javaName)}SearchFormView;
    @Resource
    private ${table.javaName}Service ${util.firstLower(table.javaName)}Service;

    @Resource
    private ${table.javaName}EditController ${util.firstLower(table.javaName)}EditController;

    private ${table.javaName}Where ${util.firstLower(table.javaName)}Where = null;
    private boolean isInitSearchable = false;

    public void addData(ActionEvent event) {
        Region region = (Region) ${util.firstLower(table.javaName)}EditFormView.getView();
        ${util.firstLower(table.javaName)}EditController.cleanData();
        LxDialog ldhDialog = new LxDialog(StageUtil.STAGE, "add", region.getPrefWidth(), region.getPrefHeight());
        ldhDialog.setContentPane(region);
        ldhDialog.setStyle("-fx-padding: 2");
        ldhDialog.show();
    }

    public void editData(ActionEvent event) {
        TableView<${table.javaName}> tableView = ${util.firstLower(table.javaName)}GridTable.getTableView();
        ${table.javaName} selectedItem = tableView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            DialogUtil.info("提示", "请选择一行", 300, 120);
            return;
        }
        Region region = (Region) ${util.firstLower(table.javaName)}EditFormView.getView();
        ${util.firstLower(table.javaName)}EditController.setInitData(selectedItem);
        LxDialog ldhDialog = new LxDialog(StageUtil.STAGE, "修改", region.getPrefWidth(), region.getPrefHeight());
        ldhDialog.setContentPane(region);
        ldhDialog.setStyle("-fx-padding: 2");
        ldhDialog.show();
    }

    public void removeData(ActionEvent event) {
        TableView<${table.javaName}> tableView = ${util.firstLower(table.javaName)}GridTable.getTableView();
        ${table.javaName} selectedItem = tableView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            DialogUtil.info("提示", "请选择一行", 300, 120);
            return;
        }
    }

    public void searchData(ActionEvent event) {
        if (!isInitSearchable) {
            Region region = (Region) ${util.firstLower(table.javaName)}SearchFormView.getView();
            ${util.firstLower(table.javaName)}GridTable.getMasterDetailPane().setDetailNode(region);
            isInitSearchable = true;
        }
        ${util.firstLower(table.javaName)}GridTable.expandPane();
    }

    public void setSearchParam(${table.javaName}Where searchParam) {
        this.${util.firstLower(table.javaName)}Where = searchParam;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ${util.firstLower(table.javaName)}GridTable.setLoadData(this);
        load(null);
    }

    @Override
    public void load(Pageable pageable) {
        ${util.firstLower(table.javaName)}GridTable.getMaskerPane().setVisible(true);

        task(()->{
            PageResult<${table.javaName}> pageResult = loadPageResult(pageable);
            return null;
        });
    }

    private void loadingEnd(Task task) {
                ${util.firstLower(table.javaName)}GridTable.getMaskerPane().textProperty().unbind();
                ${util.firstLower(table.javaName)}GridTable.getMaskerPane().progressProperty().unbind();
                ${util.firstLower(table.javaName)}GridTable.getMaskerPane().setVisible(false);
        if (task.getException() != null) {
            task.getException().printStackTrace();
            DialogUtil.info("错误", task.getException().getMessage(), 300, 220);
        }
    }

    private PageResult<${table.javaName}> loadPageResult(Pageable pageable) {
        try {
            if (${util.firstLower(table.javaName)}Where == null) ${util.firstLower(table.javaName)}Where = new ${table.javaName}Where();
            if (pageable != null) {
                ${util.firstLower(table.javaName)}Where.setPageSize(pageable.getPageSize());
                ${util.firstLower(table.javaName)}Where.setPageNo(pageable.getPageNo());
            }
            PageResult<${table.javaName}> pageResult = ${util.firstLower(table.javaName)}Service.findBy${table.javaName}Where(${util.firstLower(table.javaName)}Where);
            Platform.runLater(()->${util.firstLower(table.javaName)}GridTable.setData(pageResult));
            return pageResult;
        } catch (Exception var2) {
            DialogUtil.info("查询数据失败", var2.getMessage(), 300, 220);
            return null;
        }
    }

    public void saveData(${table.javaName} ${util.firstLower(table.javaName)}, Function<?, ?> function) {
        task(()->{
            try {
                if (${util.firstLower(table.javaName)}.get${util.firstUpper(table.primaryKey.column.property)}() == null) {
                    ${util.firstLower(table.javaName)}Service.insert(${util.firstLower(table.javaName)});
                } else {
                    ${util.firstLower(table.javaName)}Service.updateBy${util.firstUpper(table.primaryKey.column.property)}(${util.firstLower(table.javaName)});
                }
                function.apply(null);
            } catch (Exception var2) {
                DialogUtil.info("保存数据失败", var2.getMessage(), 300, 220);
                return null;
            }

            ${table.javaName}MainController.this.loadPageResult((Pageable)null);
//            updateMessage("保存数据成功");
            return null;
        });
    }

    private void deleteData(${table.javaName} ${util.firstLower(table.javaName)}) {
        task(()->{
//          updateMessage("正在删除数据");

            try {
                ${table.primaryKey.column.javaType} id = ${util.firstLower(table.javaName)}.get${util.firstUpper(table.primaryKey.column.property)}();
                ${util.firstLower(table.javaName)}Service.deleteBy${util.firstUpper(table.primaryKey.column.property)}(id);
            } catch (Exception var2) {
                DialogUtil.info("删除数据失败", var2.getMessage(), 300, 220);
                return null;
            }

            ${table.javaName}MainController.this.loadPageResult((Pageable)null);
//           updateMessage("删除数据成功");
            return null;
        });
    }

    private void task(Supplier<?> supplier) {
        Task<Void> task = new Task() {
            @Override
            protected Void call() throws Exception {
                supplier.get();
                return null;
            }
        };
        task.setOnSucceeded(event -> {loadingEnd(task);});
        task.setOnFailed(event->{loadingEnd(task);});
        new Thread(task).start();
    }
}
