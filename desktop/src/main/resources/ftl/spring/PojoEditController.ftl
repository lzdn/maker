package ${controllerPackage}.${util.firstLower(table.javaName)};

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.WindowEvent;
import ldh.fx.util.DialogUtil;
import ldh.fx.component.table.function.ValuedEnumStringConverter;
import org.apache.commons.lang3.StringUtils;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import ${pojoPackage}.${table.javaName};
<#list table.columnList as column>
    <#if util.isEnum(column)>
import ${enumProjectPackage}.${column.javaType};
    </#if>
</#list>

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
* Created by ldh on 2017/4/17.
*/
@FXMLController
public class ${table.javaName}EditController implements Initializable{

    ValidationSupport validationSupport = new ValidationSupport();

    @Resource
    private ${table.javaName}MainController ${util.firstLower(table.javaName)}MainController;

    <#list table.columnList as column>
        <#if util.isDate(column)>
    @FXML private DatePicker ${column.property}DatePicker;
        <#elseif util.isEnum(column)>
    @FXML private ChoiceBox ${column.property}ChoiceBox;
        <#else>
    @FXML private TextField ${column.property}Text;
        </#if>
    </#list>
    @FXML private Button cancelBtn;

    private ${table.primaryKey.column.javaType} ${table.primaryKey.column.property};

    @FXML private void closeAct() {
        cancelBtn.getScene().getWindow().fireEvent(new WindowEvent(cancelBtn.getScene().getWindow(), WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML private void submitAct() {
        if(validationSupport.isInvalid()) {
            DialogUtil.info("填写错误", "请按照要求填写", 300, 120);
            return;
        }
        ${table.javaName} ${util.firstLower(table.javaName)} = buildBean();
        ${util.firstLower(table.javaName)}MainController.saveData(${util.firstLower(table.javaName)}, (Void)->{doAfterSuccess(); return null;});
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        <#list table.columnList as column>
            <#if util.isEnum(column)>
        ${column.property}ChoiceBox.getItems().addAll(${column.javaType}.values());
        <#--${column.property}ChoiceBox.setConverter(new ValuedEnumStringConverter());-->
            </#if>
        </#list>

        <#list table.columnList as column>
            <#if util.isDate(column)>
        validationSupport.registerValidator(${column.property}DatePicker, false, (Control c, LocalDate newValue) ->
            ValidationResult.fromWarningIf(${column.property}DatePicker, "不能为空", newValue == null));
            <#elseif util.isEnum(column)>
        validationSupport.registerValidator(${column.property}ChoiceBox, Validator.createEmptyValidator("${column.property}不能为空"));
            <#elseif util.isBigDecimal(column)>
        validationSupport.registerValidator(${column.property}Text, Validator.createRegexValidator("${column.property}为double", "^(([1-9][0-9]*)|((([1-9][0-9]*)|0)\\.?[0-9]{1,5}?))$", Severity.ERROR));
            <#elseif util.isNumber(column)>
        validationSupport.registerValidator(${column.property}Text, Validator.createRegexValidator("${column.property}为数字", "^[0-9]*$", Severity.ERROR));
            <#else>
        validationSupport.registerValidator(${column.property}Text, Validator.createEmptyValidator("${column.property}不能为空"));
            </#if>
        </#list>
    }

    public void setInitData(${table.javaName} data) {
        if (data == null) {
            return;
        }

        ${table.primaryKey.column.property} = data.get${util.firstUpper(table.primaryKey.column.property)}();
        <#list table.columnList as column>
            <#if util.isDate(column)>
        if (data.get${util.firstUpper(column.property)}() != null) {
            LocalDateTime ${column.property}DateTime = LocalDateTime.ofInstant(data.get${util.firstUpper(column.property)}().toInstant(), ZoneId.systemDefault());
            ${column.property}DatePicker.setValue(${column.property}DateTime.toLocalDate());
        }
            <#elseif column.foreign && !column.foreignKey.foreignTable.primaryKey.composite>
        if (data.get${util.firstUpper(column.property)}() != null) {
            ${column.property}Text.setText(data.get${util.firstUpper(column.property)}().get${util.firstUpper(column.foreignKey.table.primaryKey.columns[0].property)}() + "");
        }
            <#elseif util.isNumber(column)>
        if (data.get${util.firstUpper(column.property)}() != null) {
            ${column.property}Text.setText(data.get${util.firstUpper(column.property)}() + "");
        }
            <#elseif util.isEnum(column)>
        if (data.get${util.firstUpper(column.property)}() != null) {
            ${column.property}ChoiceBox.getSelectionModel().select(data.get${util.firstUpper(column.property)}() + "");
        }
            <#else>
        if (data.get${util.firstUpper(column.property)}() != null) {
            ${column.property}Text.setText(data.get${util.firstUpper(column.property)}() + "");
        }
            </#if>
        </#list>
    }

    public void cleanData() {
        <#list table.columnList as column>
            <#if util.isDate(column)>
        ${column.property}DatePicker.setValue(null);
            <#elseif column.foreign && !column.foreignKey.foreignTable.primaryKey.composite>
        ${column.property}Text.setText("");
            <#elseif util.isNumber(column)>
        ${column.property}Text.setText("");
            <#elseif util.isEnum(column)>
        ${column.property}ChoiceBox.getSelectionModel().select(null);
            <#else>
        ${column.property}Text.setText("");
            </#if>
        </#list>
    }

    public ${table.javaName} buildBean(){
        ${table.javaName}  ${util.firstLower(table.javaName)} = new ${table.javaName}();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = null;
        <#list table.columnList as column>
            <#if util.isDate(column)>
        LocalDate ${column.property}Date = ${column.property}DatePicker.getValue();
        zdt = ${column.property}Date.atStartOfDay(zoneId);
        ${util.firstLower(table.javaName)}.set${util.firstUpper(column.property)}(Date.from(zdt.toInstant()));
            <#elseif util.isPrimaryKey(table, column)>

            <#elseif util.isEnum(column)>
        ${column.javaType} ${column.property} = (${column.javaType}) ${column.property}ChoiceBox.getSelectionModel().getSelectedItem();
        if (${column.property} != null) {
            ${util.firstLower(table.javaName)}.set${util.firstUpper(column.property)}(${column.property});
        }
            <#elseif column.foreign>
//            <#--paramMap.put("${column.property}.${column.foreignKey.foreignTable.primaryKey.columns[0].property}", ${column.property}Text.getText());-->
            <#elseif util.isBigDecimal(column)>
        if (!StringUtils.isEmpty((${column.property}Text.getText().trim()))) {
            ${util.firstLower(table.javaName)}.set${util.firstUpper(column.property)}(new BigDecimal(${column.property}Text.getText().trim()));
        }
            <#else>
        ${util.firstLower(table.javaName)}.set${util.firstUpper(column.property)}(${column.javaType}.valueOf(${column.property}Text.getText()));
            </#if>
        </#list>

        ${util.firstLower(table.javaName)}.set${util.firstUpper(table.primaryKey.column.property)}(${table.primaryKey.column.property});
        return ${util.firstLower(table.javaName)};
    }

    public void doAfterSuccess() {
        Platform.runLater(()->closeAct());
    }
}
