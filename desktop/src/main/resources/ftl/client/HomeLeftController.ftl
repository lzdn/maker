package ${projectPackage}.controller;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeLeftController implements Initializable {

    private HomeController homeController;

    public void setHomeController(HomeController homeController) {
        this.homeController = homeController;
    }

    @FXML
    public void tongleLeftPane(ActionEvent e) {
        homeController.tongleLeftPane();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        <#--userPopupPane = new LPopup(userBtn, PopupPos.down_west);-->
        <#--userPopupPane.setPopupContentPane(userContent);-->
    }

    @FXML public void showDashboard() {
        System.out.println("show dashboard!!!!!!!!");
    }

    @FXML public void leftNaveClick(ActionEvent e) {
        Node parent = (Node) e.getSource();
        if (parent instanceof Button) {
            Object userData = ((Button) parent).getUserData();
            if (userData == null) return;
            try {
                String fxml = "/fxml/module/" + firstLower(userData.toString()) + "/" + userData+ "Main.fxml";
                Pane parent1 = FXMLLoader.load(this.getClass().getResource(fxml));
                changeContent(parent1);
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
    }

    private void changeContent(Node node) {
        if (homeController.getContentPane().getChildren().size() < 1) return;
        Node oldNode = homeController.getContentPane().getChildren().get(0);
        FadeTransition ft = new FadeTransition(Duration.millis(500), oldNode);
        ft.setFromValue(1.0);
        ft.setToValue(0.01);
        ft.setOnFinished(e->{
            homeController.getContentPane().getChildren().clear();
            homeController.getContentPane().getChildren().add(node);
            VBox.setVgrow(node, Priority.ALWAYS);
            homeController.getContentPane().requestLayout();
        });

        FadeTransition ft2 = new FadeTransition(Duration.millis(800), node);
        ft2.setFromValue(0.01);
        ft2.setToValue(1.0);
        SequentialTransition sequentialTransition=new SequentialTransition(ft,ft2);
        sequentialTransition.setCycleCount(1);
        sequentialTransition.play();
    }

    private String firstLower(String str) {
        if (str == null || str.trim().equals("")) return str;
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
}
