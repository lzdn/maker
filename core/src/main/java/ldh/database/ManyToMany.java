package ldh.database;

public class ManyToMany {

	private Table primaryTable;
	
	private ForeignKey primaryForeignKey;
	
	private Table secondTable;
	
	private ForeignKey secondForeignKey;
	
	private Table middleTable;
	
	public ManyToMany(Table primaryTable, ForeignKey primaryForeignKey, Table secondTable, ForeignKey secondForeignKey, Table middleTable) {
		this.primaryTable = primaryTable;
		this.secondTable = secondTable;
		this.middleTable = middleTable;
		this.primaryForeignKey = primaryForeignKey;
		this.secondForeignKey = secondForeignKey;
	}

	public Table getPrimaryTable() {
		return primaryTable;
	}

	public void setPrimaryTable(Table primaryTable) {
		this.primaryTable = primaryTable;
	}

	public Table getSecondTable() {
		return secondTable;
	}

	public void setSecondTable(Table secondTable) {
		this.secondTable = secondTable;
	}

	public Table getMiddleTable() {
		return middleTable;
	}

	public void setMiddleTable(Table middleTable) {
		this.middleTable = middleTable;
	}

	public ForeignKey getPrimaryForeignKey() {
		return primaryForeignKey;
	}

	public void setPrimaryForeignKey(ForeignKey primaryForeignKey) {
		this.primaryForeignKey = primaryForeignKey;
	}

	public ForeignKey getSecondForeignKey() {
		return secondForeignKey;
	}

	public void setSecondForeignKey(ForeignKey secondForeignKey) {
		this.secondForeignKey = secondForeignKey;
	}

	public boolean equals(ManyToMany mtm) {
		return this.middleTable.getName().equals(mtm.getMiddleTable().getName());
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof ManyToMany)) {
			return false;
		}
		return equals((ManyToMany) obj);
	}
	
	@Override
	public int hashCode() {
		return this.middleTable.getName().hashCode();
	}
}
