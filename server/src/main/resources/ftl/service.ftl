package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

@Service("${service}")
public class ${className} <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if>{

	@Resource
	private ${daoMaker.simpleName} ${util.javaName(daoMaker.simpleName)};
	
	<#------------------insert-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insert") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional
	public ${bean} insert(${bean} ${util.javaName(bean)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.insert(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} insert error!");
		}
		return ${util.javaName(bean)};
	}
	
	</#if>
	<#------------------update-------------------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if index.primaryKey>
	<#if util.isCreate(table, "updateBy${util.uniqueName(index)}") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional
	public ${bean} updateBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)}) {
		int  n = ${util.javaName(daoMaker.simpleName)}.updateBy${util.uniqueName(index)}(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} update error!");
		}
        return ${util.javaName(bean)};
     }

	</#if>
	</#if>
	<#------------------delete------------------------------------------------------------------------------------------>
	<#if util.isCreate(table, "deleteBy${util.uniqueName(index)}") >
	<#if implements?exists>
    @Override
	</#if>
    @Transactional
    public void deleteBy${util.uniqueName(index)}(${util.uniqueParam(index)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.deleteBy${util.uniqueName(index)}(${util.uniqueValue(index)});
		if (n != 1) {
			throw new RuntimeException("${bean} delete error!");
		}
    }

	</#if>
	</#list>
	<#-----------------------------------getUnique----------------------------------------------------------------------------------->
	<#list table.indexies as index>	
	<#if util.isUnique(index, table)>
	<#if util.isCreate(table, "getBy" + util.uniqueName(index))>
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getBy${util.uniqueName(index)}(${util.uniqueParam(index)}) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getBy${util.uniqueName(index)}(${util.uniqueValue(index)});
		return ${util.javaName(bean)};
    }
    
    </#if>
    </#if>
	</#list>
	<#------------------getManyToMany----------------------------------------------------------------------------------->
	<#if table.manyToManys??>
	<#list table.manyToManys as mm>	
	<#if util.isCreate(mm.primaryTable, "get" + util.firstUpper(mm.secondTable.javaName) + "sById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getMany${util.firstUpper(mm.secondTable.javaName)}sById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getMany${util.firstUpper(mm.secondTable.javaName)}sById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	</#if>
	<#------------------getWith----------------------------------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getWith${util.firstUpper(foreignKey.column.property)}ById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getWith${util.firstUpper(foreignKey.column.property)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getWith${util.firstUpper(foreignKey.column.property)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getWithAssociatesById--------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getWithAssociatesById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getWithAssociatesById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getWithAssociatesById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	<#------------------getJoin----------------------------------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.column.property)}ById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getJoin${util.firstUpper(foreignKey.column.property)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoin${util.firstUpper(foreignKey.column.property)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getJoinAssociatesById--------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getJoinAssociatesById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getJoinAssociatesById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoinAssociatesById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	
	<#------------------getManyJoin------------------------------------------------------------------------------------->
	<#list table.many as foreignKey>	
	<#if util.isCreate(table, "getManyJoin${util.firstUpper(foreignKey.table.javaName)}ById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getManyJoin${util.firstUpper(foreignKey.table.javaName)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getManyJoin${util.firstUpper(foreignKey.table.javaName)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getManyJoinAllById------------------------------------------------------------------------------>
	<#if table.many?size gt 1>
	<#if util.isCreate(table, "getManyJoinAllById") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public ${bean} getManyJoinAllById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getManyJoinAllById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	<#------------------delete------------------------------------------------------------------------------------------>
	<#if util.isCreate(table, "delete") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional
	public void delete(${key} id) {
		${util.javaName(daoMaker.simpleName)}.delete(id);
	}
	
	</#if>
	<#------------------find-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public PageResult<${bean}> findBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)}) {
		long total = ${util.javaName(daoMaker.simpleName)}.findTotalBy${beanWhere}(${util.javaName(beanWhere)});
		List<${bean}> imageList = new ArrayList<${bean}>();
		if (total > 0) {
			imageList  = ${util.javaName(daoMaker.simpleName)}.findBy${beanWhere}(${util.javaName(beanWhere)});
		}
		
		return new PageResult<${bean}>(${util.javaName(beanWhere)}, total, imageList);
	}
	
	</#if>
	<#------------------findByJoin-------------------------------------------------------------------------------------->
    <#if util.isCreate(table, "findByJoin") >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if implements?exists>
	@Override
	</#if>
	@Transactional(readOnly = true)
	public PageResult<${bean}> findByJoin${beanWhere}(${beanWhere} ${util.javaName(beanWhere)}) {
		long total = ${util.javaName(daoMaker.simpleName)}.findTotalByJoin${beanWhere}(${util.javaName(beanWhere)});
		List<${bean}> imageList = new ArrayList<${bean}>();
		if (total > 0) {
			imageList  = ${util.javaName(daoMaker.simpleName)}.findByJoin${beanWhere}(${util.javaName(beanWhere)});
		}
		
		return new PageResult<${bean}>(${util.javaName(beanWhere)}, total, imageList);
	}
	</#if>
	</#if>
	</#if>
	
}