package ${package};

import ldh.common.json.JsonViewFactory;
import ldh.common.mybatis.ValuedEnum;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

<#if imports?exists>
    <#list imports as import>
import ${import};
    </#list>
</#if>

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/common")
public class CommonController {

    private Map<String, Class> enumMap = new HashMap<>();
    {
        <#list enumStatusMakerList as maker>
        enumMap.put("${maker.showName}", ${maker.showName}.class);
        </#list>
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all/json/{enumName}")
    @ResponseBody
    public String listJson(@PathVariable("enumName") String enumName, Model model) throws Exception{
        Class clazz = enumMap.get(enumName);
        Method method = MethodUtils.getAccessibleMethod(clazz, "values", new Class[]{});
        ValuedEnum[] ves = (ValuedEnum[]) method.invoke(null, new Object[]{});
        List<Map<String, Object>> list = new ArrayList<>();
        for (ValuedEnum ve : ves) {
            Map<String, Object> map = new HashMap<>();
            map.put("value", ve.getValue());
            map.put("desc", ve.getDesc());
            list.add(map);
        }
        return JsonViewFactory.create()
            .put("data", list)
            .toJson();
        }
}
