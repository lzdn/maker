package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class ApplicationBootMaker extends FreeMarkerMaker<ApplicationBootMaker> {

    protected String projectRootPackage;

    public ApplicationBootMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    @Override
    public ApplicationBootMaker make() {
        data();
        this.out("applicationBoot.ftl", data);
        return this;
    }

    @Override
    public void data() {
        fileName = "ApplicationBoot.java";
        data.put("projectRootPackage", projectRootPackage);
    }
}
