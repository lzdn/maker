package ldh.maker.freemaker;

import ldh.common.PageResult;
import ldh.common.json.ValuedEnumObjectSerializer;
import ldh.database.Column;
import ldh.maker.util.FreeMakerUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;

public class ControllerMaker extends BeanMaker<ControllerMaker> {
	
	private Class<?> bean;
	private BeanMaker<?> beanWhereMaker;
	private BeanMaker<?> beanMaker;
	private ServiceInterfaceMaker serviceInterfaceMaker = null;
	private ServiceMaker serviceMaker = null;
	
	public ControllerMaker() {
		imports.add(PageResult.class.getName());
		imports.add(Controller.class.getName());
		imports.add(Resource.class.getName());
		imports.add(ModelAttribute.class.getName());
		imports.add(PathVariable.class.getName());
		imports.add(RequestMapping.class.getName());
		imports.add(RequestMethod.class.getName());
		imports.add(Model.class.getName());
		imports.add(Objects.class.getName());
		imports.add("ldh.common.json.JsonViewFactory");
		imports.add(ResponseBody.class.getName());
	}

	public ControllerMaker service(ServiceInterfaceMaker sm) {
		serviceInterfaceMaker = sm;
		this.imports.add(serviceInterfaceMaker.getName());
		return this;
	}
	
	public ControllerMaker service(ServiceMaker sm) {
		serviceMaker = sm;
		this.imports.add(serviceMaker.getName());
		return this;
	}
	
	public ControllerMaker bean(Class<?> bean) {
		this.bean = bean;
		this.imports.add(bean.getName());
		return this;
	}
	
	public ControllerMaker beanWhereMaker(BeanMaker<?> beanWhereMaker) {
		this.beanWhereMaker = beanWhereMaker;
		this.imports.add(beanWhereMaker.getName());
		return this;
	}
	
	public ControllerMaker beanMaker(BeanMaker<?> beanMaker) {
		this.beanMaker = beanMaker;
		this.imports.add(beanMaker.getName());
		return this;
	}
	
	public void data() {
		check();
		if (bean != null) {
			data.put("bean", bean.getSimpleName());
			data.put("beanWhere", bean.getSimpleName() + "Where");
			data.put("lowerBean", FreeMakerUtil.firstLower(bean.getSimpleName()));
			
		}
		if (beanWhereMaker != null){
			data.put("beanWhere", beanWhereMaker.getSimpleName() + "Where");
			data.put("beanWhereMaker", beanWhereMaker);
		}
		
		if (beanMaker != null){
			data.put("bean", beanMaker.getSimpleName());
			data.put("beanMaker", beanMaker);
			data.put("lowerBean", FreeMakerUtil.firstLower(beanMaker.getSimpleName()));
		}
		if (serviceInterfaceMaker != null) {
			data.put("service", serviceInterfaceMaker);
		} else {
			data.put("service", serviceMaker);
		}

		data.put("module", "");
		
		String t = FreeMakerUtil.firstLower(table.getJavaName());
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<t.length(); i++) {
			if (Character.isUpperCase(t.charAt(i))) {
				sb.append("_");
			} 
			sb.append(t.substring(i, i+1).toLowerCase());
		}
		data.put("jspFile", sb.toString());

		if (FreeMakerUtil.hasDate(table)) {
			this.imports.add("ldh.common.mvc.DateEditor");
		}

		fileName = this.className + ".java";
		handleEnum();
		super.data();
		
	}
	
	public void check() {
		if (bean == null && beanWhereMaker == null) {
			throw new NullPointerException("bean or beanMaker is not null!!!");
		}
		if (table == null && table == null) {
			throw new NullPointerException("tabe is not null!!!");
		}
		
		if(FreeMakerUtil.hasDate(table)) {
			imports.add(Date.class.getName());
			imports.add(InitBinder.class.getName());
//			imports.add(DateEditor.class.getName());
			imports.add(ServletRequestDataBinder.class.getName());
			imports.add(HttpServletRequest.class.getName());
		}
		
		super.check();
	}
	
	@Override
	public ControllerMaker make() {
		data();
		out("controller.ftl", data);
		
		return this;
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
		
		new ControllerMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className("ScenicDao")
		 	.make();
		
	}

	
}
